metabarlist data for euka02 and arth02 available at /bettik/PROJECTS/pr-ilea/COMMON/data/multimarker_datafiles
*For in-silico analysis* gbinv files are available in /bettik/PROJECTS/pr-ilea/COMMON/meta-primer/
  gbinv_taxon_list.txt.gz list of all taxons in genbank reference sequence database at the 20240213
  gbinv_taxon_list.uniq.txt.gz list of taxons (unique occurence per taxa) in genbank reference sequence database at the 20240213
All files fo DB_check_arth_in_genbank.Rmd shloud be in /bettik/PROJECTS/pr-ilea/COMMON/meta-primer/data
