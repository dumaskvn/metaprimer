---
title: "Compare amplification between Euka02 and Arth02"
author: "Dumas Keyvan"
date: "`r Sys.Date()`"
output: html_document
---

# Loading
*all files needed or this script should be in /bettik/PROJECTS/pr-ilea/COMMON/meta-primer/data*
```{r setup, include=FALSE}
library(seqinr)
library(taxize)
library(taxizedb)
library(gsubfn)
library(parallel)
library(dplyr)
library(magrittr)
library(tidyr)
library(ROBIFastread)
library(gsubfn)
library(ggplot2)
wd<-"~/Recherche/01_these/02_ilea/02_ILEA/"
setwd(wd)
```

```{r taxid-db}
# download database
db_download_ncbi()
src_ncbi <- src_ncbi()
# Retrieve all artropods taxid
taxize::classification("Arthropoda", d='ncbi')
all.arth <- taxizedb::downstream(6656, db = "ncbi")
saveRDS(all.arth, "5a_metaprimer/data/taxid_all_arthropods.rds")
all.arth <- readRDS("5a_metaprimer/data/taxid_all_arthropods.rds") %>%
  {.[[1]]}
```

```{bash, eval=FALSE}
gzip -d ~/Recherche/01_these/02_ilea/02_ILEA/5a_metaprimer/data/gbinv/gbinv_taxon_list.uniq.txt.gz 
```

# Retrieve taxonomy of all species in gbinv

I extracted taxonomic information from bginv reference data base to measure how many taxa were present, and thus compare it with the result of amplification by euka02 an arth02 primers

```{r gbinv-avail-arth, eval=FALSE, include=FALSE}
tmp <- read.table("5a_metaprimer/data/gbinv/gbinv_taxon_list.uniq.txt", sep="=")
tmp.c <- as.character(tmp[,"V1"])
tmp.c %>% head
strsplit(head(tmp), ".", fixed = TRUE)

test <- tmp.c[grepl("Drosophila melanogaster", tmp.c)][1] 
test %>%
  {sapply(strsplit(., "\\. "), "[[", 1)} %>% 
  gsub(x = ., pattern = "^\\s+", replacement = "") %>%
  gsub(x = ., pattern = "\\.$", replacement = "")

tmp.filt <- sapply(strsplit(tmp.c, "\\. "), "[[", 1) %>% 
  gsub(x = ., pattern = "^\\s+", replacement = "") %>%
  gsub(x = ., pattern = "\\.$", replacement = "") %>%
  .[grepl("Arthropoda", .)]

x = tmp.filt %>% 
  unique %>%
  head %>%
  strsplit(., ";") %>%
  {.[[2]]} 

tmp.class <- tmp.filt %>% 
  unique %>%
#  head %>%
  strsplit(., ";") %>%
  mclapply(X = ., FUN = function(x){

    cbind(x %>% name2taxid(., out_type = "summary"),
      rank = x %>% name2taxid(., out_type = "summary") %>% {.$id} %>% taxid2rank) %>%
  {.[!{.$name %in% .$name[duplicated(.$name)]},]} %>%
  dplyr::select(name, rank) %>%
  left_join(data.frame(rank = c("kingdom", "phylum", "class", "order", "family", "genus", "species")),
            .) %>%
  pivot_wider(names_from = rank, values_from = name) %>%
  dplyr::select(kingdom, phylum, class, order, family, genus, species)

  },
  mc.cores = 7) 
dt.tmp.class <- mclapply(tmp.class, FUN = as.data.frame, mc.cores = 7) %>%
  do.call(rbind, .)
saveRDS(dt.tmp.class, "5a_metaprimer/data/gbinv/gbinv_Arthropods_smallTaxo.rds")
```

```{r}
dt.tmp.class <- readRDS("5a_metaprimer/data/gbinv/gbinv_Arthropods_smallTaxo.rds")
```

# Load DB

```{r load-euka02}
db.euka02 <- ROBIFastread::read_obifasta("/home/keyvan/Recherche/01_these/02_ilea/02_ILEA/2_metabar/DB/DB.Euka02.3.30.900.uniq.fasta")
db.euka02.annot <- db.euka02 %>%
  mutate(
    order_name = features %>%
      gsub(
        x = .,
        pattern = '"',
        replacement = ''
      ) %>%
      strapplyc(.,  'order_name:([A-z ]+),', simplify = TRUE) %>%
      lapply(., FUN = function(x) ifelse(length(x) == 0, NA, x)) %>%
      unlist,
    family_name = features %>%
      gsub(
        x = .,
        pattern = '"',
        replacement = ''
      ) %>%
      strapplyc(.,  'family_name:([A-z ]+),', simplify = TRUE) %>%
      lapply(., FUN = function(x) ifelse(length(x) == 0, NA, x)) %>%
      unlist,
    genus_name = features %>%
      gsub(
        x = .,
        pattern = '"',
        replacement = ''
      ) %>%
      strapplyc(.,  'genus_name:([A-z ]+),', simplify = TRUE) %>%
      lapply(., FUN = function(x) ifelse(length(x) == 0, NA, x)) %>%
      unlist,
    scientific_name = features %>%
      gsub(
        x = .,
        pattern = '"',
        replacement = ''
      ) %>%
      strapplyc(.,  'scientific_name:([A-z ]+),', simplify = TRUE) %>%
      lapply(., FUN = function(x) ifelse(length(x) == 0, NA, x)) %>%
      unlist
  )
db.euka02.annot %>% head
```

```{r load-arth02}
db.arth02 <- ROBIFastread::read_obifasta("/home/keyvan/Recherche/01_these/02_ilea/02_ILEA/2_metabar/DB/DB.Arth02.3.70.170.fasta")
db.arth02.annot <- db.arth02 %>%
  mutate(
    order_name = features %>%
      gsub(
        x = .,
        pattern = '"',
        replacement = ''
      ) %>%
      strapplyc(.,  'order_name:([A-z ]+),', simplify = TRUE) %>%
      lapply(., FUN = function(x) ifelse(length(x) == 0, NA, x)) %>%
      unlist,
    family_name = features %>%
      gsub(
        x = .,
        pattern = '"',
        replacement = ''
      ) %>%
      strapplyc(.,  'family_name:([A-z ]+),', simplify = TRUE) %>%
      lapply(., FUN = function(x) ifelse(length(x) == 0, NA, x)) %>%
      unlist,
    genus_name = features %>%
      gsub(
        x = .,
        pattern = '"',
        replacement = ''
      ) %>%
      strapplyc(.,  'genus_name:([A-z ]+),', simplify = TRUE) %>%
      lapply(., FUN = function(x) ifelse(length(x) == 0, NA, x)) %>%
      unlist,
    scientific_name = features %>%
      gsub(
        x = .,
        pattern = '"',
        replacement = ''
      ) %>%
      strapplyc(.,  'scientific_name:([A-z ]+),', simplify = TRUE) %>%
      lapply(., FUN = function(x) ifelse(length(x) == 0, NA, x)) %>%
      unlist
  )
db.arth02.annot %>% head
db.arth02.annot$scientific_name %>% na.omit %>% length
db.arth02.annot$scientific_name %>% na.omit %>% unique %>% length
```

# Compare
Be carefull, freq.gbinv is the number of species within each family, while freq.euka02 and freq.arth02 is the number of sequences matched by the primer, and identified to the family, some can thus belong to the same OTU

## Families
```{r compare-genbank-db-families}
fam.freq.gbinv <- dt.tmp.class$family %>% unlist %>% table %>% as.data.frame %>% rename("family" = ".", "freq.gbinv" = "Freq")
fam.freq.euka02 <- db.euka02.annot$family_name %>% unlist %>% table %>% as.data.frame %>% rename("family" = ".", "freq.euka02" = "Freq")
fam.freq.arth02 <- db.arth02.annot$family_name %>% unlist %>% table %>% as.data.frame %>% rename("family" = ".", "freq.arth02" = "Freq")

fam.freq <- left_join(fam.freq.gbinv, fam.freq.euka02,
                      by = "family") %>%
  left_join(., fam.freq.arth02, 
            by = "family")
head(fam.freq)

# number of families amplified by euka02 only
fam.freq %>%
  filter(!is.na(freq.euka02) & is.na(freq.arth02)) %>% nrow
fam.freq %>%
  filter(!is.na(freq.euka02) & is.na(freq.arth02)) %>%
  pull(family)
# number of families amplified by arth02 only
fam.freq %>%
  filter(is.na(freq.euka02) & !is.na(freq.arth02)) %>% nrow
fam.freq %>%
  filter(is.na(freq.euka02) & !is.na(freq.arth02)) %>% 
  pull(family)

fam.freq %>% na.omit %>% nrow

GGally::ggpairs(fam.freq[,2:4])
```

```{r plot-freq-family}
hist(fam.freq$freq.euka02)
hist(fam.freq$freq.arth02)

ggplot(fam.freq, aes(x = freq.arth02, y = freq.euka02)) +
  geom_point() +
  geom_smooth(method="lm") +
  scale_x_log10() +
  scale_y_log10() +
  theme_minimal()

cor(fam.freq$freq.arth02, fam.freq$freq.euka02,use = "complete.obs")
m1 <- lm(freq.euka02 ~ freq.arth02, data = fam.freq)
summary(m1)
hist(m1$residuals)

```

# Order
```{r compare-genbank-db-order}
ord.freq.gbinv <- dt.tmp.class$order %>% unlist %>% table %>% as.data.frame %>% rename("order" = ".", "freq.gbinv" = "Freq")
ord.freq.euka02 <- db.euka02.annot$order_name %>% unlist %>% table %>% as.data.frame %>% rename("order" = ".", "freq.euka02" = "Freq")
ord.freq.arth02 <- db.arth02.annot$order_name %>% unlist %>% table %>% as.data.frame %>% rename("order" = ".", "freq.arth02" = "Freq")

ord.freq <- left_join(ord.freq.gbinv, ord.freq.euka02,
                      by = "order") %>%
  left_join(., ord.freq.arth02, 
            by = "order")
head(ord.freq)

# number of families amplified by euka02 only
ord.freq %>%
  filter(!is.na(freq.euka02) & is.na(freq.arth02)) %>% nrow
ord.freq %>%
  filter(!is.na(freq.euka02) & is.na(freq.arth02)) %>%
  pull(order)
# number of families amplified by arth02 only
ord.freq %>%
  filter(is.na(freq.euka02) & !is.na(freq.arth02)) %>% nrow
ord.freq %>%
  filter(is.na(freq.euka02) & !is.na(freq.arth02)) %>% 
  pull(order)

fam.freq %>% na.omit %>% nrow

GGally::ggpairs(ord.freq[,2:4])

```

```{r plot-freq-order}
hist(ord.freq$freq.euka02)
hist(ord.freq$freq.arth02)

ggplot(ord.freq, aes(x = freq.arth02, y = freq.euka02)) +
  geom_point() +
  geom_smooth(method="lm") +
  scale_x_log10() +
  scale_y_log10() +
  theme_minimal()

cor(ord.freq$freq.arth02, ord.freq$freq.euka02,use = "complete.obs")
m2 <- lm(freq.euka02 ~ freq.arth02, data = ord.freq)
summary(m2)
hist(m1$residuals)

```
